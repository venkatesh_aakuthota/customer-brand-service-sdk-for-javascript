Feature: List Level 2 Customer Brands
  Lists all level 2 customer brands

  Scenario: Success
    Given I provide a valid accessToken
    When I execute listLevel2CustomerBrands
    Then all level 2 customer brands in the customer-brand-service are returned