import CustomerBrandView from './customerBrandView';

/**
 * @class {Level1CustomerBrandView}
 */
export default class Level1CustomerBrandView extends CustomerBrandView {

    constructor(id:number, name:string, customerSegmentId:number) {

        super(id, name, customerSegmentId);

    }

}
