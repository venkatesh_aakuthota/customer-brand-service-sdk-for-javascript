/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    firstName: 'firstName',
    lastName: 'lastName',
    customerBrandId:1,
    customerSegmentId:1,
    customerBrandName:'customerBrandName',
    accountId:'000000000000000000',
    sapVendorNumber:'sapVendorNo',
    partnerRepId:1,
    emailAddress:'email@test.com',
    url:'https://dummy-url.com'
};